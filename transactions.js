const tsjs_xps = require("tsjs-xpx-chain-sdk");

function aggregatedTransaction(keyPrivate, arrayAddress){
	//const transactionHttp = new TransactionHttp('http://localhost:3000');
	let transactionHttp = new tsjs_xps.TransactionHttp('http://bctestnet1.brimstone.xpxsirius.io:3000');

	//const privateKey = process.env.PRIVATE_KEY;
	let privateKey = keyPrivate;
	let accountTransfer = tsjs_xps.Account.createFromPrivateKey(privateKey, tsjs_xps.NetworkType.TEST_NET);

	/*
	const aliceAddress = 'VDG4WG-FS7EQJ-KFQKXM-4IUCQG-PXUW5H-DJVIJB-OXJG';
	const aliceAccount = Address.createFromRawAddress(aliceAddress);

	const bobAddress = 'VCGPXB-2A7T4I-W5MQCX-FQY4UQ-W5JNU5-F55HGK-HBUN';
	const bobAccount = Address.createFromRawAddress(bobAddress);
	*/
	
	// set public address
	let accounts = [];
	for(var i = 0; i < arrayAddress.length; i++){
		accounts.push(tsjs_xps.Address.createFromRawAddress(arrayAddress[i]));
	}
	//arrayAddress.forEach(address => tsjs_xps.Address.createFromRawAddress(address));
	
	let amount = tsjs_xps.NetworkCurrencyMosaic.createRelative(1); // 10 xpx represent 10 000 000 micro xpx

	// transferTransaction
	let transactions = [];
	for(var i = 0; i < accounts.length; i++){ //same as array, consider doing it in the same for
		transactions.push(tsjs_xps.TransferTransaction.create(tsjs_xps.Deadline.create(), accounts[i], [amount], tsjs_xps.PlainMessage.create('payout'), tsjs_xps.NetworkType.TEST_NET));
	}
	/**
	const aliceTransferTransaction = TransferTransaction.create(Deadline.create(), aliceAccount, [amount], PlainMessage.create('payout'), NetworkType.TEST_NET);
	const bobTransferTransaction = TransferTransaction.create(Deadline.create(), bobAddress, [amount], PlainMessage.create('payout'), NetworkType.TEST_NET);
	**/
	// aggregate first address to transaction
	let aggregates = [];
	for(var i = 0; i < transactions.length; i++){
		aggregates.push(transactions[i].toAggregate(accountTransfer.publicAccount));
	}
	

	let aggregateTransaction = tsjs_xps.AggregateTransaction.createComplete(
	    tsjs_xps.Deadline.create(),
	    /*
	    [
		aliceTransferTransaction.toAggregate(danAccount.publicAccount),
		bobTransferTransaction.toAggregate(danAccount.publicAccount)
	    ],
	    */
	    aggregates,
	    tsjs_xps.NetworkType.TEST_NET
	);
	
	// sign and announce transaction
	const signedTransaction = accountTransfer.sign(aggregateTransaction, tsjs_xps.generationHash);

	transactionHttp
	    .announce(signedTransaction)
	    .subscribe(x => console.log(x), err => console.error(err));
	
}

arrAdd = ["VB7UYK-A65YIG-4FEQN4-JK4ZNU-EPWDMT-3LSTNA-7ED3",
	"VD36SV-EBDTDR-C4ST67-Q2PDI5-3WFXUG-BCKTXR-HRJW"];
aggregatedTransaction("C24509EBE58B0C544FCC1E20CC965C9299FE59BB6553FBC236902B10C18924DB", arrAdd);
