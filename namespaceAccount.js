const tsjs_xps = require("tsjs-xpx-chain-sdk");
const rxjs_xps = require("rxjs");

function checkNamespaceAvailability(name){
	//const namespaceHttp = new NamespaceHttp('http://localhost:3000');
	//const namespaceHttp = new tsjs_xps.NamespaceHttp('https://bctestnet1.brimstone.xpxsirius.io');
	const namespaceHttp = new tsjs_xps.NamespaceHttp('http://bctestnet1.brimstone.xpxsirius.io:3000');
	//const namespace = new NamespaceId('foo');
	const namespace = new tsjs_xps.NamespaceId(name);

	namespaceHttp
	    .getNamespace(namespace)
	    .subscribe(namespace => console.log(namespace), err => console.error(err));
}

function registerNamespace(name, keyPrivate){
	//const transactionHttp = new TransactionHttp('http://localhost:3000');
	const transactionHttp = new tsjs_xps.TransactionHttp('http://bctestnet1.brimstone.xpxsirius.io:3000');

	//const privateKey = process.env.PRIVATE_KEY;
	const privateKey = keyPrivate;
	const account = tsjs_xps.Account.createFromPrivateKey(privateKey, tsjs_xps.NetworkType.TEST_NET);

	//const namespaceName = "foo"; //Replace with an unique namespace name
	const namespaceName = name; //Replace with an unique namespace name

	const registerNamespaceTransaction = tsjs_xps.RegisterNamespaceTransaction.createRootNamespace(
	    tsjs_xps.Deadline.create(), //time limit to wait for transaction
	    namespaceName,
	    tsjs_xps.UInt64.fromUint(100000),
	    tsjs_xps.NetworkType.TEST_NET,
	    //tsjs_xps.NetworkType.MIJIN_TEST,
	    tsjs_xps.MaxFee=2
	    );

	const signedTransaction = account.sign(registerNamespaceTransaction, tsjs_xps.generationHash);

	transactionHttp
	    .announce(signedTransaction)
	    .subscribe(x => console.log(x), err => console.error(err));
	  
	/**transactionHttp.networkProperties.then(networkProperties => {
		const signedRegisterRootNamespace = sender.sign(registerNamespaceTx, networkProperties.generationHash);

		// register root namespace
		network.announceAndWaitForConfirmation(signedRegisterRootNamespace).then(tx => {
		console.log('Confirmed:');
		console.log(tx);

		const registerSubNamespaceTx = RegisterNamespaceTransaction.createSubNamespace(
		    Deadline.create(),
		    "my-sub-namespace",
		    "my-root-namespace",
		    NetworkType.MIJIN_TEST
		)

		const signedRegisterSubnamespaceTransaction = sender.sign(registerSubNamespaceTx, networkProperties.generationHash);

			// register sub-namespace
			network.announceAndWaitForConfirmation(signedRegisterSubnamespaceTransaction).then(tx => {
			    console.log('Confirmed:');
			    console.log(tx);
			});
		});
	})
	**/
}

function setNamespace(name, privateKey){
	try{
		checkNamespaceAvailability(name);
	} catch (e) {
		console.error("-------------->No namespace has been set");
	}
	registerNamespace(name, privateKey);
}

function getNamespace(name){
	//const namespaceHttp = new NamespaceHttp('http://localhost:3000');
	//const namespaceHttp = new tsjs_xps.NamespaceHttp('http://bctestnet1.brimstone.xpxsirius.io:3000');
	const namespaceHttp = new tsjs_xps.NamespaceHttp("https://bctestnet1.brimstone.xpxsirius.io");
	//const namespaceId = new NamespaceId('foo');
	const namespaceId = new tsjs_xps.NamespaceId(name);

	namespaceHttp
	    .getNamespace(namespaceId)
	    .subscribe(namespaceInfo => console.log(namespaceInfo), err => console.error(err));
}

registerNamespace("first", "C24509EBE58B0C544FCC1E20CC965C9299FE59BB6553FBC236902B10C18924DB");
registerNamespace("first", "E0EB2C3DE4E506FB75573521BE145AF23D9857A4B95A7BB8AF26E63432810D89");
//getNamespace("first");
