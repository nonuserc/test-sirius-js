const tsjs_xps = require("tsjs-xpx-chain-sdk");
const rxjs_xps = require("rxjs/operators");

function createUserAddress(){
	const account = tsjs_xps.Account.generateNewAccount(tsjs_xps.NetworkType.TEST_NET);

	console.log('Address:', account.address.pretty());
	console.log('PrivateKey:', account.privateKey);
	console.log('PublicKey:', account.publicKey);
}

function createUserFromPrivateKey(privateKey){
	// Replace with a private key
	//const privateKey = process.env.PRIVATE_KEY;

	const account = tsjs_xps.Account.createFromPrivateKey(privateKey, tsjs_xps.NetworkType.TEST_NET);

	console.log('Address:', account.address.pretty());
	console.log('PrivateKey:', account.privateKey);
	console.log('PublicKey:', account.publicKey);
}

function getAccountInfo(raw_address){	
	//const accountHttp = new tsjs_xps.AccountHttp("http://localhost:3000");
	const accountHttp = new tsjs_xps.AccountHttp("http://bctestnet1.brimstone.xpxsirius.io:3000");

	//const address = tsjs_xps.Address.createFromRawAddress("VDISRC-HDXAWN-NIXEHG-MQXQQJ-MP6CJF-CW3PIB-RHJI");

	const address = tsjs_xps.Address.createFromRawAddress(raw_address);
	accountHttp
		.getAccountInfo(address)
		.subscribe(accountInfo => console.log(accountInfo), err => console.error(err));
}

function createWalletNoKey(){
	const password = new tsjs_xps.Password('passwordtest');

	const wallet = tsjs_xps.SimpleWallet.create("wallet-first", password, tsjs_xps.NetworkType.TEST_NET);

	const account = wallet.open(password);

	console.log("Your new account address is:", account.address.pretty(), "and its private key", account.privateKey);
}

function createWalletKey(privateKey){
	const password = new tsjs_xps.Password('passwordtest');

	// Replace with a private key
	//const privateKey = process.env.PRIVATE_KEY;
	//const privateKey = "BBAC126B3E89566C0FD805D23384CF8892CBD0C49253C1D8390DEC6DB151603E"

	const wallet = tsjs_xps.SimpleWallet.createFromPrivateKey("wallet-first", password, privateKey, tsjs_xps.NetworkType.TEST_NET);

	const account = wallet.open(password);

	console.log("Your account address is:", account.address.pretty(), "and its private key", account.privateKey);
}

function getAccountMosaic(raw_address){
	// es5
	//var { mergeMap, map } = require{'rxjs/operators');

	// es6
	//import { mergeMap, map } from 'rxjs/operators';

	//const url = "http://localhost:3000";
	const url = "https://bctestnet1.brimstone.xpxsirius.io"
	const accountHttp = new tsjs_xps.AccountHttp(url);
	const mosaicHttp = new tsjs_xps.MosaicHttp(url);
	const namespaceHttp = new tsjs_xps.NamespaceHttp(url);
	const mosaicService = new tsjs_xps.MosaicService(accountHttp, mosaicHttp, namespaceHttp);

	const address = tsjs_xps.Address.createFromRawAddress(raw_address);

	mosaicService
		.mosaicsAmountViewFromAddress(address)
		.pipe(
			rxjs_xps.mergeMap((_) => _)
		)
		.subscribe(mosaic => console.log("You have", mosaic.relativeAmount(), mosaic.fullName()),
			err => console.error(err));
}

function checkXPXSent(publicKeyOrigin,addressRecipient){
	// es5
	//var { mergeMap, map, filter, toArray } = require('rxjs/operators');
	//var vmergeMap = rxjs_xps.mergeMap;

	// es6
	//import { mergeMap, map, filter, toArray } from 'rxjs/operators';

	//const accountHttp = new tsjs_xps.AccountHttp('http://localhost:3000');
	const accountHttp = new tsjs_xps.AccountHttp('https://bctestnet1.brimstone.xpxsirius.io');

	//const originPublicKey = '7D08373CFFE4154E129E04F0827E5F3D6907587E348757B0F87D2F839BF88246';
	const originPublicKey = publicKeyOrigin;
	const originAccount = tsjs_xps.PublicAccount.createFromPublicKey(originPublicKey, tsjs_xps.NetworkType.TEST_NET);

	//const recipientAddress = 'VDG4WG-FS7EQJ-KFQKXM-4IUCQG-PXUW5H-DJVIJB-OXJG';
	const recipientAddress = addressRecipient;
	const address = tsjs_xps.Address.createFromRawAddress(recipientAddress);

	accountHttp
	    .outgoingTransactions(originAccount)
	    .pipe(
		rxjs_xps.mergeMap((_) => _), // Transform transaction array to single transactions to process them
		rxjs_xps.filter((_) => _.type === tsjs_xps.TransactionType.TRANSFER), // Filter transfer transactions
		rxjs_xps.filter((_) => _.recipient.equals(address)), // Filter transactions from to account
		rxjs_xps.filter((_) => _.mosaics.length === 1 && _.mosaics[0].id.equals(tsjs_xps.NetworkCurrencyMosaic.NAMESPACE_ID)), // Filter xpx transactions
		rxjs_xps.map((_) => _.mosaics[0].amount.compact() / Math.pow(10, tsjs_xps.NetworkCurrencyMosaic.DIVISIBILITY)), // Map only amount in xpx
		rxjs_xps.toArray(), // Add all mosaics amounts into one array
		rxjs_xps.map((_) => _.reduce((a, b) => a + b, 0))
	    )
	    .subscribe(
		total => console.log('Total xpx send to account', address.pretty(), 'is:', total),
		err => console.error(err)
	    );
}

function readTransactions(keyPublic){
	//const accountHttp = new AccountHttp('http://localhost:3000');
	const accountHttp = new tsjs_xps.AccountHttp('https://bctestnet1.brimstone.xpxsirius.io');

	//const publicKey = '7D08373CFFE4154E129E04F0827E5F3D6907587E348757B0F87D2F839BF88246';
	const publicKey = keyPublic;
	const publicAccount =  tsjs_xps.PublicAccount.createFromPublicKey(publicKey, tsjs_xps.NetworkType.TEST_NET);

	const pageSize = 10; // Page size between 10 and 100, otherwise 10

	accountHttp
	    .transactions(publicAccount, new tsjs_xps.QueryParams(pageSize))
	    .subscribe(transactions => console.log(transactions), err => console.error(err));
}



//getAccountInfo("VD36SV-EBDTDR-C4ST67-Q2PDI5-3WFXUG-BCKTXR-HRJW");
//getAccountInfo("VDISRC-HDXAWN-NIXEHG-MQXQQJ-MP6CJF-CW3PIB-RHJI");
getAccountInfo("VCEBQ4-SVCCAD-HBAF3J-AB3XJV-32MPZA-XZHY7I-W7IQ");

//getAccountMosaic("VDISRC-HDXAWN-NIXEHG-MQXQQJ-MP6CJF-CW3PIB-RHJI");
//checkXPXSent("05713D1F2D1743952A49F86CD97EA20868FCCADE3FEDA2A6A9D6C01F3A5C68F6","VDISRC-HDXAWN-NIXEHG-MQXQQJ-MP6CJF-CW3PIB-RHJI")
//readTransactions("05713D1F2D1743952A49F86CD97EA20868FCCADE3FEDA2A6A9D6C01F3A5C68F6");
